
USE datalab_de32
USE DE32_Baker_Street

-- Turn polish words in account table to english, format dates - temporary table
SELECT * FROM [order]

INSERT INTO account_bks
SELECT account_id , district_id
, CASE WHEN frequency = 'POPLATEK MESICNE' THEN 'Monthly'
	   WHEN frequency = 'POPLATEK TYDNE' THEN ' Weekly'
	   ELSE 'After transaction'
	   END AS Issuance_frequency
, CONVERT(date, CONVERT(VARCHAR(10),[date])) AS [date]
FROM account

DROP TABLE ##account_eng
SELECT * FROM ##account_eng
SELECT * FROM account_bks
DROP TABLE account_bks

--create account_bks table
CREATE TABLE account_bks
(	account_id INT NOT NULL,
	district_id INT NOT NULL,
	Issuance_frequency NVARCHAR(50) NOT NULL,
	[date] DATE NOT NULL,
	CONSTRAINT pk_accountbks_acc_id PRIMARY KEY (account_id));

--Turn order table from polish to english
SELECT * FROM [order]

INSERT INTO order_bks
SELECT order_id, account_id, bank_to, account_to, CAST(amount AS DECIMAL(18,2)) AS amount
, CASE WHEN K_symbol = 'SIPO' THEN 'Household'
	   WHEN K_symbol = 'POJISTNE' THEN 'Insurrance payment'
	   WHEN K_symbol = 'UVER' THEN 'Loan payment'
	   ELSE 'Leasing'
	   END AS characterisation_of_payment
FROM [order]

DROP TABLE ##order_eng
SELECT * FROM ##order_eng
SELECT * FROM order_bks
DROP TABLE order_bks

--create order_bks table
CREATE TABLE order_bks
(	order_id INT NOT NULL,
	account_id INT NOT NULL,
	bank_to NVARCHAR(10) NOT NULL,
	account_to INT NOT NULL,
	amount DECIMAL(18,2) NOT NULL,
	K_symbol NVARCHAR(50) NULL,
	CONSTRAINT PK_orderbks_order_id PRIMARY KEY (order_id));

--Trun transaction table from polish to english and format dates
SELECT DISTINCT operation FROM trans
SELECT DISTINCT type FROM trans
SELECT DISTINCT k_symbol FROM trans

SELECT * FROM trans_bks WHERE type = 'cash withdrawal'
SELECT * FROM trans WHERE type = 'vydaj'

INSERT INTO trans_bks
SELECT trans_id, account_id
, CONVERT(date, CONVERT(VARCHAR(10),[date])) AS [date]
, CASE WHEN type = 'PRIJEM' THEN 'Credit'
	   WHEN type = 'VYDAJ' THEN 'Withdrawal'
	   ELSE 'Cash withdrawal'
	   END AS [type]
, CASE WHEN operation = 'VYBER KARTOU' THEN 'Credit-card withdrawal'
	   WHEN operation = 'PREVOD Z UCTU' THEN 'Collection from another bank'
	   WHEN operation = 'VKLAD' THEN 'Credit in cash'
	   WHEN operation = 'PREVOD NA UCET' THEN 'Remittance to another bank'
	   WHEN operation = 'VYBER' THEN 'Cash widrawal'
	   ELSE 'Credit Interest'
	   END AS operation
, CASE WHEN type != 'PRIJEM' THEN -amount
	ELSE amount END AS in_out_money
, balance
, CASE WHEN k_symbol = 'POJISTNE' THEN 'Insurrance payment'
	   WHEN k_symbol = 'SLUZBY' THEN 'Payment for statement'
	   WHEN k_symbol = 'UROK' THEN  'Interest credited'
	   WHEN k_symbol = 'SANKC. UROK' THEN 'Sanction interest if balance -ve'
	   WHEN k_symbol = 'SIPO' THEN 'Household'
	   WHEN k_symbol = 'DUCHOD' THEN 'Old-age pension'
	   WHEN k_symbol = 'UVER' THEN 'Loan payment'
	   ELSE NULL
	   END AS k_symbol
, bank
, account
FROM trans

DROP TABLE ##trans_eng
SELECT * FROM ##trans_eng
DROP TABLE trans_bks

SELECT * FROM trans_bks
WHERE k_symbol LIKE '%statement'



--create transaction_bks table
CREATE TABLE trans_bks
(	trans_id INT NOT NULL,
	account_id INT NOT NULL,
	[date] DATE NOT NULL,
	type NVARCHAR(50) NOT NULL,
	operation NVARCHAR(50) NULL,
	trns_amount DECIMAL(18,2) NOT NULL,
	balance_after_trns DECIMAL(18,2) NOT NULL,
	k_symbol NVARCHAR(50) NULL,
	bank NVARCHAR(10) NULL,
	account INT NULL,
	CONSTRAINT PK_transbks_trans_id PRIMARY KEY (trans_id));




-- Format loan payments table
SELECT DISTINCT status FROM loan

INSERT INTO loan_bks
SELECT loan_id, account_id, CONVERT(date, CONVERT(VARCHAR(10),[date])) AS [date]
, amount
, duration AS loan_duration_months
, payments
, [status]
, CASE WHEN status = 'A' THEN 'contract finished - clean'
	   WHEN status = 'B' THEN 'contract finished - loan unpaid'
	   WHEN status = 'C' THEN 'running contract - ok so far'
	   ELSE 'running contract - client in debt'
	   END AS status_meaning
FROM loan

DROP TABLE ##loan_format
SELECT * FROM ##loan_format
SELECT * FROM loan_bks
DROP TABLE loan_bks

--create loan_bks table
CREATE TABLE loan_bks
(	loan_id INT NOT NULL,
	account_id INT NOT NULL,
	[date] DATE NOT NULL,
	amount DECIMAL(18,2) NOT NULL,
	duration_loan_mnths INT NOT NULL,
	payments DECIMAL(18,2) NOT NULL,
	[status] NCHAR(1) NOT NULL,
	status_meaning NVARCHAR(50) NOT NULL
	CONSTRAINT PK_loanbks_loan_id PRIMARY KEY (loan_id));


--format card table
SELECT * FROM card

INSERT INTO card_bks
SELECT card_id, disp_id, type
, CONVERT(date, issued) AS issue_date
FROM card

SELECT * FROM ##card_format
DROP TABLE ##card_format
DROP TABLE card_bks

SELECT * FROM card_bks


-- Creating car_bks table
CREATE TABLE card_bks
(	card_id INT NOT NULL,
	disp_id INT NOT NULL,
	type NVARCHAR(50) NOT NULL,
	issue_date DATE NOT NULL,
	CONSTRAINT PK_cardbks_card_id PRIMARY KEY (card_id));


--create district_bks table
INSERT INTO district_bks

SELECT *
FROM district



DROP TABLE district_bks

CREATE TABLE district_bks
(	district_id INT NOT NULL,
	district_name NVARCHAR(50) NOT NULL,
	region NVARCHAR(50) NOT NULL,
	num_inhabitants INT NOT NULL,
	m_less_than499 INT NOT NULL,
	m_between_500_1999 INT NOT NULL,
	m_between_2000_9999 INT NOT NULL,
	m_over_10000 INT NOT NULL,
	no_of_cities INT NOT NULL,
	ratio_urban_inhab DECIMAL(18,2) NOT NULL,
	avg_salary DECIMAL(18,2) NOT NULL,
	unemp_rate_95 DECIMAL (8,2) NULL,
	unemp_rate_96 DECIMAL (8,2) NOT NULL,
	no_entrep_per1000 INT NOT NULL,
	no_crimes_95 INT NULL,
	no_crimes_96 INT NOT NULL
	CONSTRAINT PK_districtbks_district_id PRIMARY KEY (district_id));


INSERT INTO client_bks
SELECT client_id
,birth_number
,district_id
,CASE
    WHEN CAST(SUBSTRING(CAST(birth_number AS NCHAR(6)),3,2) AS INT) > 12 THEN 'F'
    ELSE 'M'
    END AS sex
,CASE
    WHEN CAST(SUBSTRING(CAST(birth_number AS NCHAR(6)),3,2) AS INT) > 12 THEN 
        CAST('19'+CAST(birth_number - 5000 AS NCHAR(6)) AS DATE)
    ELSE
        CAST('19'+CAST(birth_number AS NCHAR(6)) AS DATE)
    END AS birth_date
,CASE
    WHEN CAST(SUBSTRING(CAST(birth_number AS NCHAR(6)),3,2) AS INT) > 12 THEN 
        FLOOR(DATEDIFF(DAY,CAST('19'+CAST(birth_number - 5000 AS NCHAR(6)) AS DATE),'1999-01-01')/365.245)
    ELSE
        FLOOR(DATEDIFF(DAY,CAST('19'+CAST(birth_number AS NCHAR(6)) AS DATE),'1999-01-01')/365.245)
    END AS age 
FROM client

SELECT * FROM client_bks
DROP TABLE client_bks


--Creating client_bks table
CREATE TABLE client_bks
(	client_id INT NOT NULL,
	birth_number INT NOT NULL,
	district_id INT NOT NULL,
	sex CHAR(1) NOT NULL,
	birth_date DATE NOT NULL,
	age INT NOT NULL,
	CONSTRAINT clientbks_client_id PRIMARY KEY (client_id));

INSERT INTO disp_bks
SELECT *
FROM disp

--create disp_bks table
CREATE TABLE disp_bks
(	disp_id INT NOT NULL,
	client_id INT NOT NULL,
	account_id INT NOT NULL,
	type NVARCHAR(50) NOT NULL,
	CONSTRAINT PK_dispbks_disp_id PRIMARY KEY (disp_id));


	WITH BSCTE AS(															
SELECT YEAR(issue_date) AS [Year], card_id
, CASE WHEN type = 'classic' THEN 'X' END AS classic
, CASE WHEN type = 'junior' THEN 'y' END AS junior
, CASE WHEN type = 'gold' THEN 'z' END AS gold
FROM card_bks
)
SELECT [year], CAST(COUNT(classic) * 100 / COUNT(card_id) AS DECIMAL(8,2))  AS classic
, CAST(COUNT(junior) * 100 / COUNT(card_id)AS DECIMAL(8,2)) AS junior
, CAST(COUNT(gold) * 100 / COUNT(card_id)AS DECIMAL(8,2)) AS gold
FROM BSCTE
GROUP BY [Year]
ORDER BY [year]


